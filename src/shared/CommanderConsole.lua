local UserInputService = game:GetService("UserInputService")
local Players = game:GetService("Players")

local component = require(script.Parent.components.CommanderConsole)
local InterfaceBuilder = require(script.Parent.InterfaceBuilder)

local player = Players.LocalPlayer

local CommanderConsole = {}

CommanderConsole.keybinds = {
    consoleToggle =  {  Enum.KeyCode.LeftShift, Enum.KeyCode.C },
    historyBack = Enum.KeyCode.Up,
    historyForward =  Enum.KeyCode.Down,
}

CommanderConsole.maxHistoryEntries = 10

CommanderConsole._interface = nil
CommanderConsole._connections = {}
CommanderConsole._commandHistory = {}
CommanderConsole._currentHistoryEntry = 1
CommanderConsole._inputBoxFocused = false

function CommanderConsole:build()
    return InterfaceBuilder.build(component)
end

function CommanderConsole:display()
    if self._interface ~= nil then
        self._interface.Enabled = true
    end
end

function CommanderConsole:hide()
    if self._interface ~= nil then
        self._interface.Enabled = false
    end
end

function CommanderConsole:displayDefaultText()
    self._interface.Holder.CommandInput.Text = string.format(
        "Type command and press <b>[ENTER]</b> to execute command.\nPress <b>[%s][%s]</b> to close this window.",
        self.keybinds.consoleToggle[1].Name,
        self.keybinds.consoleToggle[2].Name
    )
end

function CommanderConsole:historyForward()
    local newEntryIndex =  self._currentHistoryEntry +1
    local newEntry = self._commandHistory[newEntryIndex]

    if newEntry ~= nil and self._interface ~= nil then
        local inputBox = self._interface.Holder.CommandInput

        inputBox.Text = newEntry
        self._currentHistoryEntry = newEntryIndex
    end
end

function CommanderConsole:historyBackward()
    local newEntryIndex =  self._currentHistoryEntry -1
    local newEntry = self._commandHistory[newEntryIndex]

    if newEntry ~= nil and self._interface ~= nil then
        local inputBox = self._interface.Holder.CommandInput

        inputBox.Text = newEntry
        self._currentHistoryEntry = newEntryIndex
    end
end

function CommanderConsole:registerCommand(command)
    if #self._commandHistory +1 > self.maxHistoryEntries then
        table.remove(self._commandHistory, 1)
    end

    table.insert(self._commandHistory, command)
    self._currentHistoryEntry = #self._commandHistory + 1
end

function CommanderConsole:init()
    self._interface = self:build()
    self._interface.Enabled = false
    self:displayDefaultText()
    self._interface.Parent = player.PlayerGui

    local inputBeganConnection = UserInputService.InputBegan:Connect(function(input, gameProcessedEvent)
        if input.UserInputType ~= Enum.UserInputType.Keyboard then
            return
        end

        if input.KeyCode == self.keybinds.historyBack and self._inputBoxFocused then
            self:historyBackward()
        elseif input.KeyCode == self.keybinds.historyForward and self._inputBoxFocused then
            self:historyForward()
        end

        if gameProcessedEvent then
            return
        end

        -- This can be improved, but I'm tired
        -- should fix it soon
        if UserInputService:IsKeyDown(Enum.KeyCode.LeftShift) and UserInputService:IsKeyDown(Enum.KeyCode.C) then
            if self._interface.Enabled == true then
                self:hide()
            else
                self:display()
            end
        end
    end)

    local inputBox: TextBox = self._interface.Holder.CommandInput

    local focusedConnection = inputBox.Focused:Connect(function()
        self._inputBoxFocused = true
    end)

    local focusLostConnection = inputBox.FocusLost:Connect(function(enterPressed)
        self._inputBoxFocused = false

        if enterPressed then
            self:registerCommand(inputBox.Text)
            inputBox.Text = ""
        end

        local trimmedString, _ = string.gsub(inputBox.Text, " ", "")

        if string.len(trimmedString) == 0 then
            self:displayDefaultText()
        end
    end)

    table.insert(self._connections, inputBeganConnection)
    table.insert(self._connections, focusLostConnection)
    table.insert(self._connections, focusedConnection)
end

function CommanderConsole:deinit()
    for _, connection in pairs(self._connections) do
        connection:Disconnect()
    end

    self._connections = {}

    if self._interface ~= nil then
        self._interface = self._interface:Destroy()
    end
end

return CommanderConsole