-- A simplistic UILib to create user interfaces using code
local InterfaceBuilder = {}

function InterfaceBuilder._buildElement(elementBuildData, parentElement)
    local element = nil

    for elementClass, elementData in pairs(elementBuildData) do
        element = Instance.new(elementClass)

        if elementData.Properties ~= nil then
            for property, value in pairs(elementData.Properties) do
                element[property] = value
            end
        end

        if elementData.Children ~= nil then
            for index, value in pairs(elementData.Children) do

                local childElementData = {}

                childElementData[index] = value

                InterfaceBuilder._buildElement(childElementData, element)
            end
        end
    end

    if parentElement ~=  nil and element ~= nil then
        element.Parent = parentElement
    end

    return element
end

function InterfaceBuilder.build(interfaceBuildData)
    local instance = InterfaceBuilder._buildElement(interfaceBuildData)

    return instance
end

return InterfaceBuilder