return {
    ScreenGui = {
        Properties = {
            Name = "CommanderConsole",
        },

        Children = {
            Frame = {
                Properties = {
                    Name = "Holder",
                    Size = UDim2.new(.5, 0, .07, 0),
                    Position = UDim2.new(.5, 0, .5, 0),
                    AnchorPoint = Vector2.new(.5,.5),

                    BackgroundColor3 = Color3.fromRGB(54, 54, 54),
                    BorderSizePixel = 0,
                    Transparency = .5
                },

                Children = {
                    TextBox = {
                        Properties = {
                            Name = "CommandInput",
                            Size = UDim2.new(.95, 0,1, 0),
                            Position = UDim2.new(.5, 0, 0, 0),
                            AnchorPoint = Vector2.new(.5, 0),
                            BackgroundTransparency = 1,

                            RichText = true,
                            TextTransparency = 0,
                            TextColor3 = Color3.fromRGB(255, 255, 255),
                            TextSize = 15,
                            Font = Enum.Font.Gotham,
                            Text = "Type command and press <b>[ENTER]</b> to execute command.\nPress <b>[LEFT SHIFT][C]</b> to close this window.",
                        }
                    },

                    UICorner = {},
                }
            }
        }
    }
}